import numpy as np
import cpuinfo
import platform
import subprocess
import firebase_admin
from firebase_admin import db, credentials
from layer.dense import Layer_Dense
from activationre.relu import Activation_ReLU
from activationsoft.softmax import Activation_Softmax
from loss.catcross import Loss
from loss.catcross import Loss_CategoricalCrossentropy
from optimize.adam import Optimizer_Adam
from data.spiral_data import spiral_data
from plot.graf import plot_graf
import tkinter as tk
from tkinter import ttk,filedialog,simpledialog
import threading
from PIL import Image, ImageTk
import time
import psutil
from hdd import StoragePerformanceTester
from calculate import calculate_performance_score
from ttkthemes import ThemedStyle
import os



def train_model(X, y, progress_callback):
    start_time = time.time()
    message = 'Please wait...\n'
    progress_callback(message)

    #generates the layers:

    dense1 = Layer_Dense(2, 128)  
    activation1 = Activation_ReLU()
    
    dense2 = Layer_Dense(128, 128)  
    activation2 = Activation_ReLU()

    dense3 = Layer_Dense(128, 128)
    activation3 = Activation_ReLU()


    dense4 = Layer_Dense(128, 3)  # output layer with 3 neurons (one for each class)
    activation4 = Activation_Softmax()

    # forward pass with the new layer
    dense1.forward(X)
    activation1.forward(dense1.output)

    dense2.forward(activation1.output)  # new hidden layer
    activation2.forward(dense2.output)

    dense3.forward(activation2.output)  # new hidden layer
    activation3.forward(dense3.output)

    dense4.forward(activation2.output)  # output layer
    activation4.forward(dense3.output)

    #define the loss function and optimizer
    loss_function = Loss_CategoricalCrossentropy()
    optimizer = Optimizer_Adam(learning_rate=0.001)
    
    # initialize lists to store CPU metrics
    cpu_usages = []
    cpu_temperatures = []

    # training loop with additional hidden layer
    for epoch in range(1001):  # nb of epochs
        # record CPU usage
        cpu_usage = psutil.cpu_percent(interval=None)
        cpu_usages.append(cpu_usage)
        
        # record CPU temperature if available
        if hasattr(psutil, "sensors_temperatures"):
            temps = psutil.sensors_temperatures()
            if "coretemp" in temps:
                core_temps = [temp.current for temp in temps["coretemp"]]
                cpu_temperatures.append(np.mean(core_temps))

        # forward pass
        dense1.forward(X)
        activation1.forward(dense1.output)
        dense2.forward(activation1.output)
        activation2.forward(dense2.output)
        dense3.forward(activation2.output)
        activation3.forward(dense3.output)
        dense4.forward(activation3.output)
        activation4.forward(dense4.output)

        # loss calculation
        loss = loss_function.calculate(activation4.output, y)

        # backward pass
        loss_function.backward(activation4.output, y)
        activation4.backward(loss_function.dinputs)
        dense4.backward(activation4.dinputs)
        activation3.backward(dense4.dinputs)
        dense3.backward(activation3.dinputs)
        activation2.backward(dense3.dinputs)
        dense2.backward(activation2.dinputs)
        activation1.backward(dense2.dinputs)
        dense1.backward(activation1.dinputs)

        # use Adam optimizer to update weights and biases
        optimizer.update_params(dense1)
        optimizer.update_params(dense2)
        optimizer.update_params(dense3)
        optimizer.update_params(dense4)

        # output the progress
        if epoch % 100 == 0:
            predictions = np.argmax(activation4.output, axis=1)  # get predictions
            accuracy = np.mean(predictions == y)  # calculate accuracy
            message = f"Epoch: {epoch}, Loss: {loss:.3f}, Accuracy: {accuracy:.3f},%\nCPU Usage: {cpu_usage:.2f}%\n"
            progress_callback(message)
    
    end_time = time.time()
    elapsed_time = end_time - start_time

    # calculate average CPU usage and temperature
    avg_cpu_usage = np.mean(cpu_usages)
    avg_cpu_temp = np.mean(cpu_temperatures) if cpu_temperatures else None

    # calculate the performance score
    performance_score = calculate_performance_score(elapsed_time,1000/elapsed_time,15,150,5,30)
    performance_score=(performance_score+avg_cpu_usage)/2

    # display final results
    message = f"Training Time: {elapsed_time:.2f} seconds\n"
    progress_callback(message)
    message = f"Average CPU Usage: {avg_cpu_usage:.2f}%\n"
    progress_callback(message)
    if avg_cpu_temp is not None:
        message = f"Average CPU Temperature: {avg_cpu_temp:.2f}°C\n"
        progress_callback(message)
    message = f"Performance Score: {performance_score:.2f}\n"
    progress_callback(message)

    info = cpuinfo.get_cpu_info()

    #adds into database
    add_in_db_for_cpu(info['brand_raw'],avg_cpu_usage,avg_cpu_temp,performance_score)


#for knowing the name of the cpu model
def get_storage_devices_info():
    storage_info = []
    
    if platform.system() == "Windows":
        command = 'wmic diskdrive get Caption'
        result = subprocess.run(command, capture_output=True, text=True, shell=True)

        lines = result.stdout.split('\n')
        for line in lines[1:]:
            if line.strip():
                model_name = line.strip()
                storage_info.append(model_name)
    
    elif platform.system() == "Linux":
        for partition in os.popen("lsblk -d -o NAME,MODEL").readlines()[1:]:
            device_info = partition.split()
            if len(device_info) >= 2:
                device = device_info[0]
                model_name = ' '.join(device_info[1:])
                storage_info.append(model_name)
    
    return storage_info


def replace_invalid_firebase_key_chars(name):
    invalid_chars = ['.', '$', '#', '[', ']', '/', '\\', '(', ')', '@', ' ']
    for char in invalid_chars:
        name = name.replace(char, '_')  # Replacing invalid characters with underscores
    return name

#add in db functions
def add_in_db_for_cpu(name,usage,temp,score):

    name = replace_invalid_firebase_key_chars(name)
    ref=db.reference("/TYPE/CPU").child(name)
    ref.update({"aNAME":name})
    ref.update({"bUSAGE":usage})
    ref.update({"cTEMP":temp})
    ref.update({"dSCORE":score})


def add_in_db_for_ssd_hdd(name,read,write,latency,iops,gb):
        
        name=replace_invalid_firebase_key_chars(name)
        entry_id=f"{name}_{gb}"
        ref=db.reference("/TYPE/SSD_HDD").child(entry_id)
        ref.update({"aNAME":name})
        ref.update({"bWRITE":write})
        ref.update({"cREAD":read})
        ref.update({"dLATENCY":latency})
        ref.update({"eIOPS":iops})
        ref.update({"fGBTESTED":gb})
    

class App:
    def __init__(self):

        cred = credentials.Certificate("credentials.json")
        firebase_admin.initialize_app(cred, {"databaseURL": "https://dcproject-9a8be-default-rtdb.europe-west1.firebasedatabase.app/"})
        self.root = tk.Tk()
        self.root.geometry('600x600')
        self.root.title('BENCHMARK TESTER')
        self.root.resizable(False, False)
       
         
        background_img = Image.open("waterfire.jpg")
        background_img = background_img.resize((600, 600))
        self.bg_tk_img = ImageTk.PhotoImage(background_img)

        
        self.bg_label = tk.Label(self.root, image=self.bg_tk_img)
        self.bg_label.place(x=0, y=0, relwidth=1, relheight=1)

       
        cpu_img = Image.open("CPU.png")
        cpu_img = cpu_img.resize((100, 100))
        self.cpu_tk_img = ImageTk.PhotoImage(cpu_img)

        ssd_img = Image.open("SSD.jpg")
        ssd_img = ssd_img.resize((100, 100))
        self.ssd_tk_img = ImageTk.PhotoImage(ssd_img)

        
        self.button_cpu = tk.Button(
            self.root,
            image=self.cpu_tk_img,
            text="",
            cursor="hand2",
            bd=0,  
            relief=tk.FLAT,  
            command=self.button_clicked_CPU
        )
        self.button_cpu.place(relx=0.25, rely=0.5, anchor="center")

        self.button_ssd = tk.Button(
            self.root,
            image=self.ssd_tk_img,
            text="",
            cursor="hand2",
            bd=0,  
            relief=tk.FLAT,  
            command=self.button_clicked_SSD
        )
        self.button_ssd.place(relx=0.75, rely=0.5, anchor="center")

        self.button_back=tk.Button(
            self.root,
            text="Go to the starting menu",
            cursor="hand2",
            bd=0,  
            relief=tk.FLAT,  
            command=self.button_clicked_back
        )
        self.root.mainloop()

        
    
    def append_text(self, message):
        self.textbox.config(state=tk.NORMAL)
        self.textbox.insert(tk.END, message)
        self.textbox.see(tk.END)  
        self.textbox.config(state=tk.DISABLED) 

    
    def update_textbox(self):
        self.textbox.update_idletasks()  
        self.root.after(100, self.update_textbox)

    def button_clicked_back(self):
        self.textbox.pack_forget()
        self.button_back.place_forget()
        self.button_cpu.place(relx=0.25, rely=0.5, anchor="center")
        self.button_ssd.place(relx=0.75, rely=0.5, anchor="center")

    def button_clicked_SSD(self):
        self.button_cpu.pack_forget()
        self.button_ssd.pack_forget()
        self.textbox = tk.Text(self.root, height=5, width=40)
        self.textbox.pack(fill=tk.BOTH, expand=True, padx=10, pady=10)
        
        selected_dir = filedialog.askdirectory(title="Select Directory")
        var=True
        while var == True:
            if os.path.exists(selected_dir)==False and os.path.isdir(selected_dir)==False:
                selected_dir = filedialog.askdirectory(title="Select Directory")
            else: var =False
       
        file_name = simpledialog.askstring("Input", "Enter the file name:")
        var=True
        while var==True:
            if not file_name or isinstance(file_name,str)==False:
                file_name = simpledialog.askstring("Input", "Enter the file name:")
            else: var=False
        
        test_file_path = os.path.join(selected_dir, file_name)

        var=True
        no_of_gb = simpledialog.askinteger("Input", "Enter the number of gigabytes:")
        while var == True:
            if no_of_gb is None or isinstance(no_of_gb,int)==False:
              no_of_gb = simpledialog.askinteger("Input", "Enter the number of gigabytes:")
            else: var = False

        total_bytes = no_of_gb * 1024 * 1024 * 1024
        iterations = 1000  

        tester = StoragePerformanceTester(test_file_path)
        self.append_text(f"Testing for {no_of_gb} GBs\n")

        write=tester.test_write_speed(total_bytes)
        read=tester.test_read_speed()
        lat=tester.test_latency(iterations)
        iops=tester.test_iops(iterations)

        self.root.update_idletasks()  
        self.append_text(write+"\n")
        self.append_text(read+"\n")
        self.append_text(lat+"\n")
        self.append_text(iops+"\n")
        self.root.update_idletasks()

        storage_devices = get_storage_devices_info()
        add_in_db_for_ssd_hdd(storage_devices[0],read,write,lat,iops,no_of_gb)

        if os.path.exists(test_file_path):
            os.remove(test_file_path)
        
        self.button_back.place(relx=0.5, rely=0.5, anchor="center",relwidth=0.4, relheight=0.2)
        self.button_back.lift()

    def button_clicked_CPU(self):
        self.button_cpu.pack_forget()
        self.button_ssd.pack_forget()
        self.textbox = tk.Text(self.root, height=5, width=40, font=12)
        self.textbox.pack(fill=tk.BOTH, expand=True, padx=10, pady=(0,200))
        classes = 3
        points = 100
        X, y = spiral_data(points, classes)
    
        train_thread = threading.Thread(target=self.train_and_show_button, args=(X, y))
        train_thread.start()  

        self.textbox.config(state=tk.DISABLED)

    def show_button_back(self):
        self.button_back.place(relx=0.5, rely=0.8, anchor="center", relwidth=0.4, relheight=0.2)
        self.button_back.lift()

    def train_and_show_button(self, X, y):
        train_model(X, y, self.append_text)

        self.textbox.config(state=tk.NORMAL)

        self.root.after(0, self.show_button_back) 
        

        

if __name__ == "__main__":
    App()
