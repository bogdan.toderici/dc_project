﻿using System.Diagnostics;
//NOTE: using() in our case (file handling): automatic try...catch and despose (basically, auto-fclose)
class StoragePerformanceTester
{
    private const int BufferSize = 1024 * 1024; //1 MB buffer, default is 4 MB
    private readonly string filePath;

    public StoragePerformanceTester(string filePath)
    {
        this.filePath = filePath;
    }

    public void TestWriteSpeed(long totalBytes)
    {
        byte[] buffer = new byte[BufferSize];
        new Random().NextBytes(buffer); //Fill the buffer with random stuff

        using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, BufferSize, FileOptions.WriteThrough)) //WriteThrough ensures consistency(write the data directly to drive and skips cache, also fixes a bug, thanks ChatGPT
        {
            Stopwatch sw = Stopwatch.StartNew();
            for (long i = 0; i < totalBytes; i += BufferSize)
            {
                fs.Write(buffer, 0, buffer.Length);
            }
            sw.Stop();
            Console.WriteLine($"Write Speed: {totalBytes / 1024.0 / 1024.0 / sw.Elapsed.TotalSeconds} MB/s"); //Write speed: Number of MB written / Total time elapsed
        }
    }

    public void TestReadSpeed()
    {
        byte[] buffer = new byte[BufferSize];

        using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, BufferSize, FileOptions.SequentialScan | FileOptions.WriteThrough)) //SequentialScan ensures that will be accessed from the beginning to end
        {
            Stopwatch sw = Stopwatch.StartNew();
            while (fs.Read(buffer, 0, buffer.Length) > 0) { } //Just read the whole file
            sw.Stop();
            FileInfo fi = new FileInfo(filePath);
            Console.WriteLine($"Read Speed: {fi.Length / 1024.0 / 1024.0 / sw.Elapsed.TotalSeconds} MB/s"); //Read speed: File size(no of MB) / Total time elapsed
        }
    }

    //Latency: time that takes the storage takes to complete a read/write request
    public void TestLatency(int iterations)
    {
        byte[] buffer = new byte[1]; //1 byte buffer for latency test

        using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, 1, FileOptions.WriteThrough))
        {
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < iterations; i++) //Just write in a file 1 byte n times while measuring and summing the time for each iteration
            {
                sw.Start();
                fs.Write(buffer, 0, buffer.Length);
                fs.Flush();
                sw.Stop();
            }
            Console.WriteLine($"Average Latency: {sw.Elapsed.TotalMilliseconds / iterations} ms"); //Latency: Total time / Number of iterations
        }
    }

    //IOPS: Input/Output operations per second
    public void TestIOPS(int iterations)
    {
        byte[] buffer = new byte[4096]; //4 KB buffer for IOPS test
        new Random().NextBytes(buffer);

        using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, 4096, FileOptions.WriteThrough))
        {
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < iterations; i++) //Same as the latency test but with a different final formula
            {
                sw.Start();
                fs.Write(buffer, 0, buffer.Length);
                fs.Flush();
                sw.Stop();
            }
            Console.WriteLine($"IOPS: {iterations / sw.Elapsed.TotalSeconds}"); //IOPS Score: Number of iterations / Total time elapsed
        }
    }
}

class StoragePerformanceTesterClient
{
    static void Main(string[] args)
    {
        //NOTE: Right now, the path is written by keyboard. Will be implemented in the interface (Some kind of string builder, we will see)
        if (args.Length < 2)
        {
            Console.WriteLine("Usage: StoragePerformanceTester <testFilePath> <noOfGigabytes>");
            return;
        }

        string testFilePath = args[0];
        long noOfGB = Convert.ToInt64(args[1]);
        long totalBytes = noOfGB * 1024 * 1024 * 1024;
        int iterations = 1000; //1000 iterations as default, may change it to be customisable

        StoragePerformanceTester tester = new StoragePerformanceTester(testFilePath);

        Console.WriteLine($"Testing for {noOfGB}GBs");
        tester.TestWriteSpeed(totalBytes);
        tester.TestReadSpeed();
        tester.TestLatency(iterations);
        tester.TestIOPS(iterations);

        //Clean up test file
        if (File.Exists(testFilePath))
        {
            File.Delete(testFilePath);
        }
    }
}
