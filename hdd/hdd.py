import os
import random
import time
import sys

class StoragePerformanceTester:
    BUFFER_SIZE = 1024 * 1024  # 1 MB buffer

    def __init__(self, file_path):
        self.file_path = file_path

    def test_write_speed(self, total_bytes):
        buffer = bytearray(random.getrandbits(8) for _ in range(self.BUFFER_SIZE))

        with open(self.file_path, 'wb', buffering=0) as fs:
            start_time = time.time()
            for _ in range(0, total_bytes, self.BUFFER_SIZE):
                fs.write(buffer)
            elapsed_time = time.time() - start_time
            return f"Write Speed: {total_bytes / 1024 / 1024 / elapsed_time} MB/s"

    def test_read_speed(self):
        buffer = bytearray(self.BUFFER_SIZE)

        with open(self.file_path, 'rb', buffering=0) as fs:
            start_time = time.time()
            while fs.readinto(buffer) > 0:
                pass
            elapsed_time = time.time() - start_time
            file_size = os.path.getsize(self.file_path)
            return f"Read Speed: {file_size / 1024 / 1024 / elapsed_time} MB/s"

    def test_latency(self, iterations):
        buffer = bytearray(1)

        with open(self.file_path, 'wb', buffering=0) as fs:
            start_time = time.time()
            for _ in range(iterations):
                fs.write(buffer)
                fs.flush()
            elapsed_time = time.time() - start_time
            return f"Average Latency: {(elapsed_time * 1000 / iterations)*1000} micros"

    def test_iops(self, iterations):
        buffer = bytearray(random.getrandbits(8) for _ in range(4096))

        with open(self.file_path, 'wb', buffering=0) as fs:
            start_time = time.time()
            for _ in range(iterations):
                fs.write(buffer)
                fs.flush()
            elapsed_time = time.time() - start_time
            return f"IOPS: {iterations / elapsed_time}"

