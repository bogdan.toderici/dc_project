import matplotlib.pyplot as plt
import numpy as np


def plot_graf(classes,X,y):

    plt.figure(figsize=(8, 6))  
    colors = ["red", "green", "blue"]

    for class_number in range(classes):
        indices = np.where(y == class_number)
        plt.scatter(X[indices, 0], X[indices, 1], color=colors[class_number], label=f"Class {class_number}")

    plt.title("Spiral Data with Different Colors for Each Class")
    plt.xlabel("X Axis")
    plt.ylabel("Y Axis")

    plt.legend()

    plt.show()
