import numpy as np

class Activation_ReLU:
    def forward(self, inputs):
        self.inputs = inputs
        self.output = np.maximum(0, inputs)#if neuron's output is <0 => outputs=0
    
    def backward(self, dvalues):#goes back 
        self.dinputs = dvalues.copy()
        self.dinputs[self.inputs <= 0] = 0