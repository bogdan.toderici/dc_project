import numpy as np

#optimize the learning rate of the ai and also modifies the weights to improve the ai
class Optimizer_Adam:
    def __init__(self, learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7):
        self.learning_rate = learning_rate
        self.beta_1 = beta_1
        self.beta_2 = beta_2
        self.epsilon = epsilon
        self.iterations = 0
        self.momentum = {}
        self.cache = {}

    def update_params(self, layer):
        if layer not in self.momentum:
            self.momentum[layer] = {"dweights": np.zeros_like(layer.weights), "dbiases": np.zeros_like(layer.biases)}
            self.cache[layer] = {"dweights": np.zeros_like(layer.weights), "dbiases": np.zeros_like(layer.biases)}

        self.iterations += 1
        lr_t = self.learning_rate * np.sqrt(1 - self.beta_2 ** self.iterations) / (1 - self.beta_1 ** self.iterations)

        self.momentum[layer]["dweights"] = self.beta_1 * self.momentum[layer]["dweights"] + (1 - self.beta_1) * layer.dweights
        self.momentum[layer]["dbiases"] = self.beta_1 * self.momentum[layer]["dbiases"] + (1 - self.beta_1) * layer.dbiases

        self.cache[layer]["dweights"] = self.beta_2 * self.cache[layer]["dweights"] + (1 - self.beta_2) * (layer.dweights ** 2)
        self.cache[layer]["dbiases"] = self.beta_2 * self.cache[layer]["dbiases"] + (1 - self.beta_2) * (layer.dbiases ** 2)

        layer.weights -= lr_t * self.momentum[layer]["dweights"] / (np.sqrt(self.cache[layer]["dweights"]) + self.epsilon)
        layer.biases -= lr_t * self.momentum[layer]["dbiases"] / (np.sqrt(self.cache[layer]["dbiases"]) + self.epsilon)
