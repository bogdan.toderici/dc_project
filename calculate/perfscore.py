#it has the score between 0 and 100
#throughput = nb of op/training_time

def calculate_performance_score(training_time, throughput, min_training_time, max_training_time, min_throughput, max_throughput):
    if training_time < min_training_time:
        training_time = min_training_time
    if training_time > max_training_time:
        training_time = max_training_time
    if throughput < min_throughput:
        throughput = min_throughput
    if throughput > max_throughput:
        throughput = max_throughput

    normalized_time = (max_training_time - training_time) / (max_training_time - min_training_time)
    normalized_throughput = (throughput - min_throughput) / (max_throughput - min_throughput)

    performance_score = 100 * normalized_time * normalized_throughput
    return performance_score